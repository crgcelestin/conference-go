from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Attendee
from events.models import Conference
from django.views.decorators.http import require_http_methods

class AttendeeListEncoder(ModelEncoder):
    model=Attendee
    properties=["name"]
class AttendeeDetailEncoder(ModelEncoder):
    model=Attendee
    properties=[
        "name",
        "email",
        "company_name",
        "created",
    ]
    def get_extra_data(self, o):
        return {"conference":o.conference.name}

@require_http_methods(["GET","POST"])
def api_list_attendees(request, conference_id):
    if request.method=="GET":
        attendees=Attendee.objects.all()
        return JsonResponse(
        {"attendees":attendees},
        encoder=AttendeeListEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            conference=Conference.objects.get(id=conference_id)
            content["conference"]=conference
            attendee=Attendee.objects.create(**content)
            return JsonResponse(
                attendee,
                encoder=AttendeeDetailEncoder,
                safe=False,
            )
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid conference id"},
                status=400,
            )


@require_http_methods(["DELETE","GET","PUT"])
def api_show_attendee(request, pk):
    if request.method=="GET":
        attendees=Attendee.objects.get(id=pk)
        return JsonResponse(
        attendees,
        encoder=AttendeeDetailEncoder,
        safe=False,
        )
    elif request.method=="DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count>0})
    else:
        content=json.loads(request.body)
        try:
            conference=Conference.objects.get(id=pk)
            content["conference"]=conference
            attendee=Attendee.objects.create(**content)
            return JsonResponse(
                attendee,
                encoder=AttendeeDetailEncoder,
                safe=False,
            )
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid conference id"},
                status=400,
            )
        Attendee.objects.filter(id=pk).update(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
