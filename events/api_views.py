from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
from .acls import get_photo, get_weather_data

class StateDetailEncoder(ModelEncoder):
	model=State
	properties=[
	    "name",
		"id",
		"abbreviation",
    ]
class StateListEncoder(ModelEncoder):
    model=State
    properties=["name"]

class LocationDetailEncoder(ModelEncoder):
    model=Location
    properties=[
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self,o):
        return {"state":o.state.abbreviation}

class LocationListEncoder(ModelEncoder):
    model=Location
    properties=["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders={
        "location":LocationListEncoder(),
    }

class ConferenceListEncoder(ModelEncoder):
    model=Conference
    properties=[
        "name",
    ]

@require_http_methods(["GET","POST"])
def api_list_conferences(request):
    if request.method=="GET":
        conferences = Conference.objects.all()
        return JsonResponse(
        {"conferences": conferences},
        encoder=ConferenceListEncoder,
    )
    else:
        content=json.loads(request.body)
        try:
            location=Location.objects.get(id=content["location"])
            content["location"]=location
            conference=Conference.objects.create(**content)
            return JsonResponse(
               conference,
               encoder=ConferenceDetailEncoder,
               safe=False,
            )
        except Location.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Location"},
                status=400,
            )

@require_http_methods(["DELETE","GET","PUT"])
def api_show_conference(request, pk):
    if request.method=="GET":
        conference = Conference.objects.get(id=pk)
        # Use the city and state abbreviation of the Conference's Location
        # to call the get_weather_data ACL function and get back a dictionary
        # that contains the weather data
        content['city']=city
        state=State.objects.get(abbreviation=content["state"])
        content["state"]=state
        weather=get_weather_data.create.object(city,state)
        # Include the weather data in the JsonResponse (see it in the dictionary?)
        return JsonResponse(
            {"conference":conference, "weather":weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method=="DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count>0})
    else:
        content=json.loads(request.body)
        try:
            location=Location.objects.get(id=content["location"])
            content["location"]=location
            conference=Conference.objects.create(**content)
            return JsonResponse(
               conference,
               encoder=ConferenceDetailEncoder,
               safe=False,
            )
        except Location.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Location"},
                status=400,
            )
        Conference.objects.filter(id=pk).update(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET","POST"])
def api_list_locations(request):
    if request.method=="GET":
        locations = Location.objects.all()
        return JsonResponse(
        {"location": locations},
        encoder=LocationListEncoder,
    )
    else:
        content=json.loads(request.body)
        try:
            state=State.objects.get(abbreviation=content["state"])
            content["state"]=state
        except State.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid state abbreviation"},
                status=400,
            )
        #use city, state abbreviation in content dict
        #to call get_photo ACL function
        content['city']=city
        state=State.objects.get(abbreviation=content["state"])
        content["state"]=state
        get_photo(city,state)
        #use returned dict to update dict content

        location=Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET","PUT"])
def api_show_location(request, pk):
    if request.method=="GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
        #handle delete method
    elif request.method=="DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count>0})
    else:
        #Put method
        content=json.loads(request.body)
        try:
            state=State.objects.get(abbreviation=content["state"])
            content["state"]=state
            location=Location.objects.create(**content)
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
        )
        except State.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=pk).update(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
