#contain code used to make HTTP requests to Pexels, Open Weather Map
#Code should return Python dictionaries to be used by view code
#use vars in code when making requests
import json
import requests
from .keys import PEXELS_API_KEYS, OPEN_WEATHER_API_KEY

#write two functions (1) makes request using Pexels API, (2) make a request to Open Weather API
#use both functions in views

#use pexels api
#make one api call using get_photo and integrate with DB / headers, param, make call
def get_photo(city,state):
    headers = {'Authorization': PEXELS_API_KEYS}
    params= {
        "query":f'{city} {state}'
    }
    url='https://api.pexels.com/v1/search'
    response=requests.get(url,params=params,headers=headers)
    content=json.loads(response.content)
    print(content)

#use open weather api
#make two api calls using get_weather_data
def get_weather_data(city,state):
    headers = {'Authorization': OPEN_WEATHER_API_KEY}
    params={
        "q": f'{city} {state}'
    }
    url="http://api.openweathermap.org/geo/1.0/direct"
