from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
import json
from events.models import Conference
from django.views.decorators.http import require_http_methods


class StatusDetailEncoder(ModelEncoder):
    model=Status
    properties=[
        "id",
        "name",
    ]
class StatusListEncoder(ModelEncoder):
    model=Status
    properties=["name"]
class PresentationListEncoder(ModelEncoder):
    model=Presentation
    properties=["title","status"]
    def get_extra_data(self, o):
        return {"status":o.status.name}

class PresentationDetailEncoder(ModelEncoder):
    model=Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["GET","POST"])
def api_list_presentations(request, conference_id):
    if request.method=="GET":
        presentation=Presentation.objects.all()
        return JsonResponse(
            {"presentation":presentation},
            encoder=PresentationListEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            conference=Conference.objects.get(id=conference_id)
            content["conference"]=conference
            presentation=Presentation.objects.create(**content)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid conference"},
                status=400,
            )


@require_http_methods(["DELETE","GET","PUT"])
def api_show_presentation(request, pk):
    if request.method=="GET":
        presentation=Presentation.objects.get(id=pk)
        return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
    elif request.method=="DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({"deleted":count>0})
    else:
        content=json.loads(request.body)
        try:
            conference=Conference.objects.get(id=conference_id)
            content["conference"]=conference
            presentation=Presentation.objects.create(**content)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid conference"},
                status=400,
            )
        Presentation.objects.filter(id=pk).update(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
